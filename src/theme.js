export default {
  colors: {
    text: '#111',
    background: '#efefef',
    hoverHightlight: 'lightgreen',
    primary: 'hotpink',
    secondary: 'turquoise'
  }
}
